from django.urls import path
from .views import BookListCreate, BookRetrieveUpdateDestroy

urlpatterns = [
    path(
        "books", BookListCreate.as_view(), name="book-list-create"
    ),  # URL pattern for listing and creating books
    path(
        "books/<int:pk>",
        BookRetrieveUpdateDestroy.as_view(),
        name="book-retrieve-update-destroy",
    ),  # URL pattern for retrieving, updating, and deleting a book by ID
]
