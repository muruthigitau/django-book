from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse
from .models import Book, Author


class BookstoreApiTests(TestCase):
    def setUp(self):
        # Initializing the test client
        self.client = APIClient()
        # Getting the URL for listing and creating books
        self.books_url = reverse("book-list-create")
        # Getting the URL for retrieving, updating, and deleting books (assuming pk=1 exists)
        self.book_detail_url = reverse("book-retrieve-update-destroy", kwargs={"pk": 1})
        # Creating authors for testing
        self.author_1 = Author.objects.create(name="Author 1")
        self.author_2 = Author.objects.create(name="Author 2")
        # Creating books for testing
        self.book_1 = Book.objects.create(
            title="Book 1", author=self.author_1, publishedYear=2020
        )
        self.book_2 = Book.objects.create(
            title="Book 2", author=self.author_2, publishedYear=2021
        )

    def test_list_books(self):
        # Test case for listing books
        response = self.client.get(self.books_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Checking if the correct number of books is returned
        self.assertEqual(
            len(response.data), 2
        )  # Assuming we have 2 books in the database

    def test_create_book(self):
        # Test case for creating a new book
        data = {
            "title": "New Book",
            "author": "Author 3",  # Assuming Author 3 exists
            "publishedYear": 2022,
        }
        response = self.client.post(self.books_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_retrieve_book(self):
        # Test case for retrieving a book
        response = self.client.get(
            reverse("book-retrieve-update-destroy", kwargs={"pk": self.book_1.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Checking if the retrieved book's title matches
        self.assertEqual(response.data["title"], "Book 1")

    def test_update_book(self):
        # Test case for updating a book
        data = {
            "title": "Updated Book",
            "author": "Author 1",  # Assuming Author 1 exists
            "publishedYear": 2020,
        }
        response = self.client.put(
            reverse("book-retrieve-update-destroy", kwargs={"pk": self.book_1.pk}),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Checking if the updated book's title matches
        self.assertEqual(response.data["title"], "Updated Book")

    def test_delete_book(self):
        # Test case for deleting a book
        response = self.client.delete(
            reverse("book-retrieve-update-destroy", kwargs={"pk": self.book_1.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
