from django.db import models


class Author(models.Model):
    """
    The Author model represents an author of a book. It contains fields for
    storing the author's name.
    """

    name = models.CharField(max_length=255)  # The name of the author

    def __str__(self):
        """
        This method returns the string representation of the author, which is their name.
        This is useful when querying the database and displaying author objects.
        """
        return self.name


class Book(models.Model):
    """
    The Book model represents a book in the library. It contains fields for
    storing the title, the author (as a ForeignKey), and the year the book was published.
    """

    title = models.CharField(max_length=255)  # The title of the book
    author = models.ForeignKey(
        Author, on_delete=models.CASCADE, null=True
    )  # The author of the book
    publishedYear = models.IntegerField()  # The year the book was published

    def __str__(self):
        """
        This method returns the string representation of the book, which is its title.
        This is useful when querying the database and displaying book objects.
        """
        return self.title
