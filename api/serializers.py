from rest_framework import serializers
from .models import Book, Author


class AuthorSerializer(serializers.ModelSerializer):
    """
    Serializer for the Author model.
    """

    class Meta:
        model = Author
        fields = ["id", "name"]


class BookSerializer(serializers.ModelSerializer):
    """
    Serializer for the Book model.
    """

    author = AuthorSerializer()  # AuthorSerializer for the author field

    class Meta:
        model = Book
        fields = ["id", "title", "author", "publishedYear"]
