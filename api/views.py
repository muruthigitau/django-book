from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny
from django.shortcuts import get_object_or_404
from .models import Author, Book
from .serializers import BookSerializer
from rest_framework.exceptions import NotFound, ValidationError


class BookListCreate(APIView):
    """
    This view handles GET and POST requests for the Book model.

    - GET: Returns a list of all books.
    - POST: Creates a new book.
    """

    permission_classes = [AllowAny]  # Allow any user to access this view

    def get(self, request):
        """
        Handles GET requests to retrieve a list of all books.
        """
        try:
            books = Book.objects.all()  # Fetch all books from the database
            serializer = BookSerializer(books, many=True)  # Serialize the book data
            return Response(serializer.data)  # Return the serialized data
        except Exception as e:
            # Handle any unexpected errors
            return Response(
                {"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

    def post(self, request):
        """
        Handles POST requests to create a new book.
        """
        try:
            # Extract book data from the request
            title = request.data.get("title")
            author_data = request.data.pop("author", None)
            published_year = request.data.get("publishedYear")

            # Fetch or create the author
            author = author_fetch_create(author_data)

            # Create the book instance
            book = Book.objects.create(
                title=title, author=author, publishedYear=published_year
            )

            return Response(
                {
                    "id": book.id,
                    "title": book.title,
                    "author": book.author.name,
                    "publishedYear": book.publishedYear,
                },
                status=status.HTTP_201_CREATED,
            )  # Return the created book data

        except Exception as e:
            # Handle any unexpected errors
            return Response(
                {"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )


class BookRetrieveUpdateDestroy(APIView):
    """
    This view handles GET, PUT, PATCH, and DELETE requests for a single Book model instance.

    - GET: Retrieves a specific book by its ID.
    - PUT: Updates a book by its ID.
    - PATCH: Partially updates a book by its ID.
    - DELETE: Deletes a book by its ID.
    """

    permission_classes = [AllowAny]  # Allow any user to access this view

    def get_object(self, pk):
        """
        Helper method to retrieve a book by its primary key (ID).
        Raises a NotFound exception if the book does not exist.
        """
        try:
            return get_object_or_404(
                Book, pk=pk
            )  # Fetch the book from the database or return a 404 error
        except NotFound:
            # Raise a NotFound exception with a custom error message
            raise NotFound(detail="Book not found", code=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk):
        """
        Handles GET requests to retrieve a specific book by its ID.
        """
        try:
            book = self.get_object(pk)  # Retrieve the book instance
            serializer = BookSerializer(book)  # Serialize the book data
            return Response(serializer.data)  # Return the serialized data
        except NotFound as e:
            # Handle the book not found error
            return Response({"error": str(e)}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            # Handle any unexpected errors
            return Response(
                {"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

    def update_book(self, book, data):
        """
        Helper method to update a book instance with the provided data.
        """
        # Check if 'author' data is provided
        author_data = data.get("author", None)
        if author_data:
            author = author_fetch_create(author_data)
            # Update the author of the book
            book.author = author

        # Update the fields present in the request data
        for field in data:
            if field != "author":  # Skip updating the author field
                setattr(book, field, data[field])

        # Save the partially updated book to the database
        book.save()

    def put(self, request, pk):
        """
        Handles PUT requests to fully update a specific book by its ID.
        """
        try:
            book = self.get_object(pk)  # Retrieve the book instance
            self.update_book(book, request.data)  # Update the book with request data
            serializer = BookSerializer(book)  # Serialize the updated book
            return Response(serializer.data)  # Return the updated book data

        except ValidationError as e:
            # Handle validation errors
            return Response({"error": e.detail}, status=status.HTTP_400_BAD_REQUEST)

        except NotFound as e:
            # Handle the book not found error
            return Response({"error": str(e)}, status=status.HTTP_404_NOT_FOUND)

        except Exception as e:
            # Handle any unexpected errors
            return Response(
                {"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

    def patch(self, request, pk):
        """
        Handles PATCH requests to partially update a specific book by its ID.
        """
        try:
            book = self.get_object(pk)  # Retrieve the book instance
            self.update_book(book, request.data)  # Update the book with request data
            return Response(
                {"message": "Book updated successfully"}, status=status.HTTP_200_OK
            )

        except ValidationError as e:
            # Handle validation errors
            return Response({"error": e.detail}, status=status.HTTP_400_BAD_REQUEST)

        except NotFound as e:
            # Handle the book not found error
            return Response({"error": str(e)}, status=status.HTTP_404_NOT_FOUND)

        except Exception as e:
            # Handle any unexpected errors
            return Response(
                {"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

    def delete(self, request, pk):
        """
        Handles DELETE requests to delete a specific book by its ID.
        """
        try:
            book = self.get_object(pk)  # Retrieve the book instance
            book.delete()  # Delete the book from the database
            return Response(
                status=status.HTTP_204_NO_CONTENT
            )  # Return a 204 No Content response
        except NotFound as e:
            # Handle the book not found error
            return Response({"error": str(e)}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            # Handle any unexpected errors
            return Response(
                {"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )


def author_fetch_create(author_data):
    # If author_data is provided as a dictionary
    if hasattr(author_data, "keys"):
        # Check if it contains 'id' or 'name' fields
        if "id" in author_data:
            author_id = author_data["id"]
            author = Author.objects.filter(id=author_id).first()
        elif "name" in author_data:
            author_name = author_data["name"]
            author = Author.objects.filter(name=author_name).first()
        else:
            author = None
    else:
        # If author_data is provided as a string or number
        # Try to find the author by ID or name
        if isinstance(author_data, int):
            author = Author.objects.filter(id=author_data).first()
        else:
            author = Author.objects.filter(name=author_data).first()

    # If author does not exist, create a new one
    if not author:
        # If author_data is a dictionary, extract name field
        if hasattr(author_data, "keys") and "name" in author_data:
            name = author_data["name"]
        else:
            name = str(author_data)

        # Create the author instance
        author = Author.objects.create(name=name)

    return author
