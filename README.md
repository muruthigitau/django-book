# Bookstore API

This API enables management of books and authors within a bookstore.

## Getting Started

To get started, follow the instructions below to clone the repository and set up the Django project.

### Cloning the Repository

1. Clone the repository to your local machine using Git:

   ```bash
   git clone https://gitlab.com/muruthigitau/django-book.git
   ```

### Installation

1. Navigate to the project directory:

   ```bash
   cd django-book
   ```

2. Install the required dependencies using pip:

   ```bash
   pip install -r requirements.txt
   ```

3. Run the database migrations to create the necessary database schema:

   ```bash
   python manage.py migrate
   ```

4. Start the development server:

   ```bash
   python manage.py runserver
   ```

The development server should now be running, and you can begin interacting with the API.

## API Endpoints

### Books

#### List all Books

- **URL**: `/books/`
- **Method**: `GET`
- **Description**: Retrieves a list of all books.
- **Request Parameters**: None
- **Response**: An array of book objects. Each book object includes:
  - `id` (integer): Unique identifier for the book.
  - `title` (string): Title of the book.
  - `author` (string): Author's name.
  - `publishedYear` (integer): Publication year.

#### Create a Book

- **URL**: `/books/`
- **Method**: `POST`
- **Description**: Creates a new book.
- **Request Body**: JSON object containing:
  - `title` (string): Title of the book.
  - `author` (string): Author's name.
  - `publishedYear` (integer): Publication year.
- **Response**: JSON object representing the created book, including:
  - `id` (integer): Unique identifier for the book.
  - `title` (string): Title of the book.
  - `author` (string): Author's name.
  - `publishedYear` (integer): Publication year.

#### Retrieve, Update, Delete a Book

- **URL**: `/books/<id>/`
- **Methods**: `GET`, `PUT`, `PATCH`, `DELETE`
- **Description**:
  - `GET`: Retrieves details of a specific book by its ID.
  - `PUT`: Updates all fields of a specific book by its ID.
  - `PATCH`: Partially updates a specific book by its ID.
  - `DELETE`: Deletes a specific book by its ID.
- **Request Parameters**: `id` (integer) - ID of the book.
- **Response**:
  - `GET`: JSON object representing the book.
  - `PUT`, `PATCH`: JSON object representing the updated book.
  - `DELETE`: No content.
